function limpa_formulario_cep() {
    // Limpa valores do formulário de cep.
    $("#rua").val("");
    $("#bairro").val("");
    $("#cidade").val("");
    $("#uf").val("");
    $("#cep_message").hide();
    //$("#ibge").val("");
}

$("#cep").blur(function() {
    //Nova variável "cep" somente com dígitos.
    var cep = $(this).val().replace(/\D/g, '');
    //Verifica se campo cep possui valor informado.
    if (cep != "") {

        //Expressão regular para validar o CEP.
        var validacep = /^[0-9]{8}$/;

        //Valida o formato do CEP.
        if(validacep.test(cep)) {

            //Preenche os campos com "..." enquanto consulta webservice.
            $("#rua").val("...");
            $("#bairro").val("...");
            $("#cidade").val("...");
            $("#uf").val("...");
            //$("#ibge").val("...");

            //Consulta o webservice viacep.com.br/
            $.getJSON("http://viacep.com.br/ws/"+ cep +"/json/?callback=?", function(dados) {
                if (!("erro" in dados)) {
                    //Atualiza os campos com os valores da consulta.
                    $("#rua").val(dados.logradouro);
                    $("#bairro").val(dados.bairro);
                    $("#cidade").val(dados.localidade);
                    $("#uf").val(dados.uf);
                    $("#cep_message").show();
                    $("#cep_message").text("CEP válido");
                    $("#cep_message").css("color", "#4dcb5f");
                    //$("#ibge").val(dados.ibge);
                } //end if.
                else {
                    //CEP pesquisado não foi encontrado.
                    limpa_formulario_cep();
                    //alert("CEP não encontrado.");
                    $("#cep_message").show();
                    $("#cep_message").css("color","red");
                    $("#cep_message").text("CEP não encontrado");
                }
            });
        } //end if.
        else {
            //cep é inválido.
            limpa_formulario_cep();
            //$("#cep_message").show();
            //$("#cep_message").css("color","red");
            //$("#cep_message").text("CEP inválido");

            
            //alert("Formato de CEP inválido.");
        }
    } //end if.
    else {
        //cep sem valor, limpa formulário.
        limpa_formulario_cep();
       //

        //$("#cep_message").show();
        //$("#cep_message").css("color","red");
    }
    
});

$(document).ready(function(){
  $("#cep").mask("99.999.999");
  $("#cep_message").hide();
});

