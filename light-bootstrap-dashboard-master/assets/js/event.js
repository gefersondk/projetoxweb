$(function () {
    $('[data-toggle="popover"]').popover();
    //Datapicker

    flatpickr(".flatpickr", {
        enableTime: true,
        minDate: new Date(),
        "locale": "pt",
        time_24hr: true,
        minuteIncrement: 1,
        dateFormat: "d/m/Y H:i"
    });

    //Money mask

    $('.currency').maskMoney();

    $("#preco").keyup(function () {
        if (($(this).val() != "R$ 0.00") || $(this).val() == "") {
            $("#gratuita").hide();
        } else {
            $("#gratuita").show();
        }
    });

    //Crop Image
    var $upload_bnt = $('#upload');
    var $image_modal = $("#image");

    $upload_bnt.on('click', function () {
        this.value = null;
        $image_modal.cropper({
            background: false,
            minCropBoxWidth: 50,
            movable: false,
            zoomable: false,
            rotatable: false,
            scalable: false,
            aspectRatio: 1,
            viewMode: 1
        });

    });

    $upload_bnt.change( function () {
        changeImage(this, function () {
            $('#modalCapa').modal('show');
        });
    });

    function changeImage(input, fnCallback) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                fnCallback();
                $('#modalCapa').on('shown.bs.modal', function () {
                    $image_modal.cropper('replace', e.target.result);
                });
            };
            reader.readAsDataURL(input.files[0]);
        }

    }

    $('#modalCapa').on('hidden.bs.modal', function () {
        $image_modal.cropper('destroy');
    });

    $("#salvarImagemCapa").click(function () {
        $("#image_container").attr('src', $image_modal.cropper("getCroppedCanvas").toDataURL());
    });


});