$(function() {
    var CelMask = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
    },
    Options = {
              onKeyPress: function(val, e, field, options) {
                  field.mask(CelMask.apply({}, arguments), options);
                }
            };
    $('#phone').mask(CelMask, Options);
});

validarIgualdadeSenha = function(){
    nova_senha = $("#nova_senha").val();
    confirmar_senha = $("#confirmar_senha").val();
   
    if(!(nova_senha == confirmar_senha)){
        $("#senha_message").show();
        $("#senha_message").css("color","red");
        $("#senha_message").text("Senhas Diferentes");
        return false;
    }
    else{
        $("#senha_message").show();
        $("#senha_message").css("color","green");
        $("#senha_message").text("Senhas Iguais");
        return true;
    }
    
}

validarTamanhosenha = function(){
    nova_senha = $("#nova_senha").val();
    if(nova_senha.length > 5){
        return true;
    }else{
        $("#senha_message").show();
        $("#senha_message").css("color","red");
        $("#senha_message").text("Senha deve possuir mais de 5 caracteres");
        return false;
    }
}

$("#confirmar_senha").blur(function() {
    if(validarTamanhosenha()){
        validarIgualdadeSenha();
    }
});

$("#nova_senha").blur(function() {
    if(validarTamanhosenha()){
        $("#senha_message").hide();
        if($("#confirmar_senha").val() !== ""){
            validarIgualdadeSenha();
        }
    }
});

$('#reset_password').on('hidden.bs.modal', function () {
        $("#senha_message").hide();
        $("#senha_atual").val("");
        $("#nova_senha").val("");
        $("#confirmar_senha").val("");
});

$(document).ready(function(){
  $("#senha_message").hide();
});